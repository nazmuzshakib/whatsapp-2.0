import { createContext, useState, useCallback, useEffect } from "react";
import { auth } from "../services/firebase/auth";
import * as AUTH from "../services/firebase/auth";
import * as DB from "../services/firebase/firestore";
import {
  getUserFromLocalStorage,
  removeUserFromLocalStorage,
  saveUserToLocalStorage,
} from "../utils/userStorage";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const [error, setError] = useState(null);

  const startLoading = () => setIsLoading(true);
  const stopLoading = () => setIsLoading(false);
  const clearError = () => setError(null);

  const handleUserOnLogin = useCallback(async (user) => {
    const { user: existingUser } = await DB.getUserByEmail(user?.email);

    if (existingUser) {
      stopLoading();
      // saveUserToLocalStorage(existingUser);
      return setUser(existingUser);
    }

    const userInfo = {
      displayName: user.displayName || "Anonymous",
      email: user.email,
      photo: user.photoURL || "shorturl.at/bxzHR",
      uid: user.uid,
      provider: "GOOGLE",
      contacts: [],
      chatIds: [],
    };

    const { error: createNewUserError } = await DB.createNewUser(userInfo);

    if (createNewUserError) {
      stopLoading();
      return setError(createNewUserError);
    }

    // wait for 2.5 seconds before retrieving newly created user
    setTimeout(async () => {
      const { user: newUser } = await DB.getUserByEmail(userInfo.email);
      if (newUser) {
        // saveUserToLocalStorage(newUser);
        return setUser(newUser);
      }
      setError("Unable to retrieve user information at the moment");
      stopLoading();
    }, 2500);
  }, []);

  const handleUserOnLogout = useCallback(() => {
    setUser(null);
    clearError();
    stopLoading();
    removeUserFromLocalStorage();
  }, []);

  const onLoginWithGoogle = async () => {
    clearError();
    startLoading();
    const { user, error } = await AUTH.loginWithGoogle();
    if (error) {
      stopLoading();
      return setError(error);
    }
    await handleUserOnLogin(user);
  };

  const onLogout = async () => {
    await AUTH.logout();
  };

  useEffect(() => {
    const unsubscribe = AUTH.onAuthStateChanged(auth, (user) => {
      if (user) {
        // const userInStorage = getUserFromLocalStorage();
        // if (userInStorage !== null && user?.email === userInStorage?.email) {
        // return setUser(userInStorage);
        // } else {
        handleUserOnLogin(user);
        // }
      } else {
        handleUserOnLogout();
      }
    });
    return () => unsubscribe();
  }, [handleUserOnLogin, handleUserOnLogout]);

  useEffect(() => {
    if (user === null) return;
    saveUserToLocalStorage(user);
  }, [user, user?.contacts?.length]);

  return (
    <AuthContext.Provider
      value={{
        user: user === null ? {} : user,
        setUser,
        isAuthenticated: !!user,
        isLoading,
        error,
        onLoginWithGoogle,
        onLogout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
