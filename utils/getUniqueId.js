import uniqid from "uniqid";

const getUniqueId = () => uniqid();

export default getUniqueId;
