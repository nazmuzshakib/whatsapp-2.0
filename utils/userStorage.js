const USER_KEY = "whatsapp2:::user";

export const getUserFromLocalStorage = () => {
  const value = window?.localStorage?.getItem(USER_KEY);
  if (value === null) return null;
  try {
    return JSON.parse(value);
  } catch (e) {
    console.log("Issue parsing user from local storage", e);
    return null;
  }
};

export const saveUserToLocalStorage = (user) => {
  if (!user) {
    console.log("Unable to save invalid user to local storage");
    return;
  }
  const value = JSON.stringify(user);
  window?.localStorage?.setItem(USER_KEY, value);
};

export const removeUserFromLocalStorage = () => {
  window?.localStorage?.removeItem(USER_KEY);
};
