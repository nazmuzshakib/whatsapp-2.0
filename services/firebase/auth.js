import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import firebaseApp from "./firebase";

const auth = getAuth(firebaseApp);

const loginWithGoogle = () => {
  return signInWithPopup(auth, new GoogleAuthProvider())
    .then((result) => {
      return {
        user: result?.user,
      };
    })
    .catch((error) => {
      console.log(error);
      return {
        error:
          error.message ||
          "Issue logging with Google at the moment. Try again later",
      };
    });
};

const logout = () => {
  return signOut(auth)
    .then(() => {
      return {
        isLoggedOut: true,
      };
    })
    .catch((error) => {
      console.log(error);
      return {
        error: error?.message || "Issue with logging out",
      };
    });
};

export { auth, loginWithGoogle, onAuthStateChanged, logout };
