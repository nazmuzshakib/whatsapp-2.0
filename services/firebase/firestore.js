import {
  doc,
  getDoc,
  getFirestore,
  setDoc,
  updateDoc,
} from "firebase/firestore";
import firebaseApp from "./firebase";

const createNewUser = (userInfo = {}) => {
  const {
    displayName,
    email,
    photo,
    uid,
    contacts = [],
    chatIds = [],
    provider,
  } = userInfo;

  if (!displayName || !email || !photo || !uid || !provider) {
    return {
      error: "Please pass in all user info needed to create new user",
    };
  }

  return setDoc(doc(db, "users", email), {
    displayName,
    email,
    photo,
    uid,
    provider,
    contacts,
    chatIds,
  })
    .then(() => {
      return {};
    })
    .catch((error) => {
      console.log({ error });
      return {
        error:
          error?.message ||
          "Issue creating a new user at the moment. Try again later",
      };
    });
};

const updateUser = async (email, updatedUser = {}) => {
  console.log({ updatedUser });
  const userRef = doc(db, "users", email.toString().trim().toLowerCase());
  return updateDoc(userRef, {
    ...updatedUser,
  })
    .then(() => {
      return {};
    })
    .catch((error) => {
      console.log({ error });
      return {
        error:
          error?.message ||
          "Issue updating user at the moment. Try again later",
      };
    });
};

const getUserByEmail = async (email) => {
  if (!email || typeof email !== "string" || !email.length) {
    return {
      error: "Please provide a valid email address to retrieve user",
    };
  }
  const docRef = doc(db, "users", email);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    const user = docSnap.data();
    return {
      user,
    };
  } else {
    return {
      error: `No account associated with ${email}`,
    };
  }
};

const createNewChat = (newChat = {}) => {
  const { messages, receiverEmails, id } = newChat;

  if (
    !id ||
    !id.length ||
    !messages ||
    !messages.length ||
    !receiverEmails ||
    !receiverEmails.length
  ) {
    return {
      error:
        "Please provide a valid `id`, `messages` and a list of `receiverEmails` to create a new chat",
    };
  }

  return setDoc(doc(db, "chats", id), {
    messages,
    receiverEmails,
  })
    .then(() => {
      return {};
    })
    .catch((error) => {
      console.log({ error });
      return {
        error:
          error?.message ||
          "Issue creating a new chat at the moment. Try again later",
      };
    });
};

const getChatById = async (chatId) => {
  if (!chatId || typeof chatId !== "string" || !chatId.length) {
    return {
      error: "Please provide a valid `chatId` to retrieve chat",
    };
  }
  const docRef = doc(db, "chats", chatId);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    const chat = docSnap.data();
    return {
      chat,
    };
  } else {
    return {
      error: `No chat associated with ${chatId}`,
    };
  }
};

const db = getFirestore(firebaseApp);

export {
  db,
  createNewUser,
  getUserByEmail,
  updateUser,
  createNewChat,
  getChatById,
};
