import {
  Box,
  Button,
  Flex,
  Heading,
  StackDivider,
  Text,
  useDisclosure,
  VStack,
} from "@chakra-ui/react";
import React, { useState, useEffect } from "react";
import {
  AiOutlineArrowRight,
  AiOutlineDelete,
  AiOutlineEdit,
  AiOutlineMessage,
} from "react-icons/ai";
import useAuth from "../../hooks/useAuth";
import useChat from "../../hooks/useChat";
import ActiveChatDrawer from "../ActiveChatDrawer/ActiveChatDrawer";

const Chat = ({ chatId, currentUserEmail, onSelectChat }) => {
  const [chat, setChat] = useState(null);

  const { getChatById } = useChat();

  useEffect(() => {
    if (chat !== null) return;
    const fetchChat = async () => {
      const { chat: fetchedChat, error: fetchedChatError } = await getChatById(
        chatId
      );
      if (fetchedChatError) return;
      else return setChat(fetchedChat);
    };
    fetchChat();
  }, [chat, chatId, getChatById, setChat]);

  console.log({ chat });

  if (chat === null) return null;

  const senders = chat.receiverEmails.filter(
    (email) => email !== currentUserEmail
  );

  return (
    <Flex justifyContent={"space-between"}>
      <Box fontSize={"1rem"} flexGrow={1}>
        <Text color={"red.400"}>
          <strong>{senders.join(", ")}</strong>
        </Text>
        <Text color="gray.500">
          <sup>{chat.messages[0]}</sup>
        </Text>
      </Box>
      <Box fontSize={"1rem"}>
        <Button
          colorScheme="gray"
          variant="ghost"
          textAlign={"left"}
          onClick={() => onSelectChat(chat)}
        >
          <AiOutlineArrowRight size="1.25rem" color="green" />
        </Button>
      </Box>
    </Flex>
  );
};

export default function Chats() {
  const {
    isOpen: isActiveChatDrawerOpen,
    onOpen: onActiveChatDrawerOpen,
    onClose: onActiveChatDrawerClose,
  } = useDisclosure();

  const [activeChat, setActiveChat] = useState(null);

  const { user = {} } = useAuth() || {};
  const { chatIds = [] } = user;

  console.log({ user, chatIds });

  return (
    <>
      <Flex className="Chats" flexDirection={"column"}>
        <Heading as="h1" size="xl">
          Chats
        </Heading>

        <VStack
          my={4}
          divider={<StackDivider borderColor="white.100" />}
          spacing={2}
          align="stretch"
        >
          {chatIds.map((chatId) => (
            <Chat
              key={chatId}
              chatId={chatId}
              currentUserEmail={user.email}
              onSelectChat={(chat) => {
                setActiveChat(chat);
                onActiveChatDrawerOpen();
              }}
            />
          ))}
        </VStack>
      </Flex>

      {activeChat && (
        <ActiveChatDrawer
          isOpen={isActiveChatDrawerOpen}
          onOpen={onActiveChatDrawerOpen}
          onClose={onActiveChatDrawerClose}
          activeChat={activeChat}
        />
      )}
    </>
  );
}
