import { Button, Flex } from "@chakra-ui/react";
import React from "react";

export const CHATS_TAB = "CHATS_TAB";
export const CONTACTS_TAB = "CONTACTS_TAB";

export default function BottomTabs({
  activeTab,
  goToChatDashboardView,
  goToContactDashboardView,
}) {
  return (
    <Flex
      className={`BottomTabs`}
      gap={2}
      position="fixed"
      bottom={0}
      left={0}
      right={0}
      m={3}
    >
      <Button
        type="button"
        colorScheme={activeTab === CHATS_TAB ? "whatsapp" : "gray"}
        flexGrow={1}
        flexShrink={1}
        flexBasis={0}
        onClick={() => goToChatDashboardView()}
      >
        Chats
      </Button>
      <Button
        type="button"
        colorScheme={activeTab === CONTACTS_TAB ? "whatsapp" : "gray"}
        flexGrow={1}
        flexShrink={1}
        flexBasis={0}
        onClick={() => goToContactDashboardView()}
      >
        Contacts
      </Button>
    </Flex>
  );
}
