import { Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import React from "react";
import { AiOutlineSearch } from "react-icons/ai";

export default function SearchContactInput() {
  return (
    <form className={`SearchContactInput`}>
      <InputGroup>
        <InputLeftElement
          pointerEvents="none"
          // eslint-disable-next-line react/no-children-prop
          children={<AiOutlineSearch color="gray" />}
        />
        <Input
          type="text"
          backgroundColor={"white"}
          shadow={"sm"}
          placeholder="Search in contacts..."
        />
      </InputGroup>
    </form>
  );
}
