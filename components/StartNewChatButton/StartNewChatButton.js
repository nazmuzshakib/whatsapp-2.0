import { Button, Text } from "@chakra-ui/react";
import React from "react";
import { AiFillPlusCircle } from "react-icons/ai";
import styles from "./StartNewChatButton.module.css";

export default function StartNewChatButton() {
  return (
    <Button
      variant="solid"
      size="sm"
      m={3}
      colorScheme="green"
      color={"white"}
      className={`StartNewChatButton ${styles.startNewChatButton}`}
    >
      <AiFillPlusCircle color="white" size={"1.5rem"} />{" "}
      <Text color={"white"} px={1}>
        New Chat
      </Text>
    </Button>
  );
}
