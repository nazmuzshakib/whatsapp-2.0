import {
  Badge,
  Box,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  Text,
} from "@chakra-ui/react";
import React from "react";

export default function ActiveChatDrawer({ isOpen, onClose, activeChat }) {
  console.log({ activeChat });
  return (
    <Drawer onClose={onClose} isOpen={isOpen} size={"full"}>
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton />
        <DrawerHeader
          backgroundColor={"green"}
          color="white"
          paddingRight={"2em"}
        >
          <Text fontSize="sm">
            {(activeChat?.receiverEmails || []).join(", ")}
          </Text>
        </DrawerHeader>
        <DrawerBody>
          <Flex justifyContent={"column"} gap={2}>
            {(activeChat?.messages || []).map((message) => {
              return (
                <Box
                  key={message}
                  p={2}
                  shadow="md"
                  borderWidth="1px"
                  minWidth={"50%"}
                  maxWidth={"75%"}
                >
                  {/* <Text>
                    <Badge>-</Badge>
                  </Text> */}
                  <Text>{message}</Text>
                </Box>
              );
            })}
          </Flex>
        </DrawerBody>
      </DrawerContent>
    </Drawer>
  );
}
