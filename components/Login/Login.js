import { Box, Button, Flex, Spinner, Text } from "@chakra-ui/react";
import { AiFillGoogleCircle } from "react-icons/ai";
import Image from "next/image";
import React from "react";

export default function Login({
  onLoginWithGoogle = () => {},
  isLoadingAuth = true,
}) {
  return (
    <Flex
      flexDirection="column"
      alignItems={"center"}
      justifyContent={"center"}
      px={2}
      py={10}
      backgroundColor={"whitesmoke"}
      height="100vh"
      className={`Login`}
      boxSizing="border-box"
    >
      {isLoadingAuth ? (
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="red.500"
          color="green.500"
          size="xl"
        />
      ) : (
        <>
          <Flex flexDirection={"column"} alignItems={"center"}>
            <Box>
              <Image
                src={"/whatsapp-logo.png"}
                width="100%"
                height={"100%"}
                alt="WhatsApp Logo"
              />
            </Box>
            <Text fontSize={"1.5rem"} fontWeight="300" color="green">
              WhatsApp 2.0
            </Text>
          </Flex>
          <Box mt={10}>
            <Button colorScheme={"red"} onClick={() => onLoginWithGoogle()}>
              <Flex justifyContent={"row"} alignItems={"center"} gap={2}>
                <AiFillGoogleCircle size={"1.5rem"} color={"white"} mr={4} />
                <Text>Log in with Google</Text>
              </Flex>
            </Button>
          </Box>
        </>
      )}
    </Flex>
  );
}
