import {
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  FormLabel,
  Input,
  InputGroup,
  InputLeftAddon,
  InputRightAddon,
  Select,
  Stack,
  Textarea,
} from "@chakra-ui/react";
import React, { useState } from "react";
import useAuth from "../../hooks/useAuth";
import useContact from "../../hooks/useContact";
import getUniqueId from "../../utils/getUniqueId";

export default function NewContactDrawer({ isOpen, onClose }) {
  const [nicknameInput, setNicknameInput] = useState("");
  const [emailInput, setEmailInput] = useState("");
  const { user, setUser } = useAuth();
  const { getAllContacts, onCreateNewContact } = useContact({ user, setUser });

  const isNicknameValid =
    nicknameInput &&
    typeof nicknameInput === "string" &&
    nicknameInput.length > 0;
  const isEmailValid =
    emailInput &&
    typeof emailInput === "string" &&
    /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(emailInput);
  const areFieldsValid = !!isNicknameValid && !!isEmailValid;

  const clearFields = () => {
    setNicknameInput("");
    setEmailInput("");
  };

  const createNewContact = () => {
    const newContact = {
      nickname: nicknameInput,
      email: emailInput,
      id: getUniqueId(),
    };
    // console.log({ newContact });
    onCreateNewContact(newContact);
    clearFields();
    onClose();
  };

  return (
    <Drawer isOpen={isOpen} placement="bottom" onClose={onClose}>
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton color={"white"} />
        <DrawerHeader
          borderBottomWidth="1px"
          backgroundColor={"green.500"}
          color="white"
        >
          New Contact
        </DrawerHeader>

        <DrawerBody>
          <Stack spacing="24px">
            <Box>
              <FormLabel htmlFor="username" color={"gray"}>
                Nickname
              </FormLabel>
              <Input
                id="name"
                placeholder="Bobbie"
                value={nicknameInput}
                onChange={(e) =>
                  setNicknameInput(e.target.value.trim().toLowerCase())
                }
                required
              />
            </Box>

            <Box>
              <FormLabel htmlFor="email" color={"gray"}>
                Email
              </FormLabel>
              <InputGroup>
                <InputLeftAddon>@</InputLeftAddon>
                <Input
                  type="email"
                  id="email"
                  placeholder="bobbie@lol.com"
                  value={emailInput}
                  onChange={(e) =>
                    setEmailInput(e.target.value.trim().toLowerCase())
                  }
                  required
                />
              </InputGroup>
            </Box>
          </Stack>
        </DrawerBody>

        <DrawerFooter>
          <Button variant="outline" mr={3} onClick={onClose}>
            Cancel
          </Button>
          <Button
            colorScheme="green"
            disabled={!areFieldsValid}
            onClick={() => createNewContact()}
          >
            Create
          </Button>
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
}
