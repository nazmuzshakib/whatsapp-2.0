import { Box, Flex } from "@chakra-ui/react";
import React from "react";
import TopBar from "../TopBar/TopBar";
import BottomTabs, { CHATS_TAB, CONTACTS_TAB } from "../BottomTabs/BottomTabs";
import { CHATS_VIEW, CONTACTS_VIEW } from "../../hooks/useView";

export default function Dashboard({
  onLogout,
  children,
  activeView,
  goToChatDashboardView,
  goToContactDashboardView,
}) {
  const getActiveTab = () => {
    switch (activeView) {
      case CHATS_VIEW:
        return CHATS_TAB;
      case CONTACTS_VIEW:
        return CONTACTS_TAB;
      default:
        return CHATS_TAB;
    }
  };

  return (
    <>
      <Flex
        className="Dashboard"
        flexDirection={"column"}
        gap={3}
        p={3}
        boxSizing="border-box"
        height="100vh"
        paddingBottom={"64px"}
        overflow="auto"
      >
        <Box>
          <TopBar onLogout={onLogout} />
        </Box>
        <Box flexGrow={1}>{children}</Box>
      </Flex>
      <BottomTabs
        activeTab={getActiveTab()}
        goToChatDashboardView={goToChatDashboardView}
        goToContactDashboardView={goToContactDashboardView}
      />
    </>
  );
}
