import {
  Box,
  Button,
  Flex,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import React from "react";
import {
  AiOutlineUser,
  AiFillPlusCircle,
  AiOutlinePoweroff,
  AiOutlineMessage,
  AiOutlineContacts,
} from "react-icons/ai";
import EditContactDrawer from "../EditContactDrawer/EditContactDrawer";
import NewChatDrawer from "../NewChatDrawer/NewChatDrawer";
import NewContactDrawer from "../NewContactDrawer/NewContactDrawer";
import SearchChatInput from "../SearchChatInput/SearchChatInput";

export default function TopBar({ onLogout }) {
  const {
    isOpen: isNewContactDrawerOpen,
    onOpen: onNewContactDrawerOpen,
    onClose: onNewContactDrawerClose,
  } = useDisclosure();
  const {
    isOpen: isNewChatDrawerOpen,
    onOpen: onNewChatDrawerOpen,
    onClose: onNewChatDrawerClose,
  } = useDisclosure();

  return (
    <Flex flexDirection={"column"} className={`TopBar`} gap={2}>
      <Flex
        justifyContent={"flex-end"}
        alignItems={"center"}
        gap={1}
        wrap={"wrap"}
      >
        <Box flexGrow={1}>
          <Text fontSize={"1.25rem"} fontWeight="300" color="green">
            WhatsApp 2.0
          </Text>
        </Box>

        <Box>
          <Menu>
            <MenuButton as={Button}>
              <AiFillPlusCircle size="1.5rem" color="green" />
            </MenuButton>
            <MenuList zIndex={3}>
              <MenuItem color={"black"} onClick={() => onNewChatDrawerOpen()}>
                <AiOutlineContacts size="1rem" color="black" />
                <Text px={2}>New Chat</Text>
              </MenuItem>
              <MenuItem
                color={"black"}
                onClick={() => onNewContactDrawerOpen()}
              >
                <AiOutlineMessage size="1rem" color="black" />
                <Text px={2}>New Contact</Text>
              </MenuItem>
            </MenuList>
          </Menu>
        </Box>

        <Box>
          <Menu>
            <MenuButton as={Button}>
              <AiOutlineUser size="1.5rem" color="black" />
            </MenuButton>
            <MenuList>
              <MenuItem color={"black"}>
                <AiOutlineUser size="1rem" color="black" />
                <Text px={2}>Account</Text>
              </MenuItem>
              <MenuItem color={"black"} onClick={() => onLogout()}>
                <AiOutlinePoweroff size="1rem" color="black" />
                <Text px={2}>Logout</Text>
              </MenuItem>
            </MenuList>
          </Menu>
        </Box>
      </Flex>

      <Box minWidth={"150px"}>
        <SearchChatInput />
      </Box>

      <NewContactDrawer
        isOpen={isNewContactDrawerOpen}
        onOpen={onNewContactDrawerOpen}
        onClose={onNewContactDrawerClose}
      />

      <NewChatDrawer
        isOpen={isNewChatDrawerOpen}
        onOpen={onNewChatDrawerOpen}
        onClose={onNewChatDrawerClose}
      />
    </Flex>
  );
}
