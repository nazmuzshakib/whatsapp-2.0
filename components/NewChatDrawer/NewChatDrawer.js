import {
  Box,
  Button,
  Checkbox,
  CheckboxGroup,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  FormLabel,
  Stack,
  Textarea,
} from "@chakra-ui/react";
import React, { useState } from "react";
import useAuth from "../../hooks/useAuth";
import useChat from "../../hooks/useChat";
import useContact from "../../hooks/useContact";
import getUniqueId from "../../utils/getUniqueId";

export default function NewChatDrawer({ isOpen, onClose }) {
  const { user } = useAuth();
  const { getAllContacts } = useContact();
  const { onCreateNewChat } = useChat();
  const [checkedContactEmails, setCheckedContactEmails] = useState([]);
  const [message, setMessage] = useState("");

  const contacts = getAllContacts();
  const isValidMessage = checkedContactEmails.length > 0 && message.length > 0;

  const onCheckboxChange = (e, contactEmail) => {
    if (e.target.checked) {
      setCheckedContactEmails((prevContactEmails) => [
        ...prevContactEmails,
        contactEmail,
      ]);
    } else {
      setCheckedContactEmails((prevContactEmails) => {
        return prevContactEmails.filter((id) => id !== contactEmail);
      });
    }
  };

  const onMessageChange = (e) => setMessage(e.target.value);

  const clearFields = () => {
    setCheckedContactEmails([]);
    setMessage("");
  };

  const onCancel = () => {
    clearFields();
    onClose();
  };

  const onSend = async () => {
    console.log({ checkedContactEmails, message });
    const { error, values } =
      (await onCreateNewChat({
        messages: [message],
        receiverEmails: [user.email, ...checkedContactEmails],
        id: getUniqueId(),
      })) || {};
    clearFields();
    onClose();
  };

  console.log({ contacts, checkedContactEmails, message });

  return (
    <Drawer isOpen={isOpen} placement="bottom" onClose={onClose}>
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton color="white" />
        <DrawerHeader
          borderBottomWidth="1px"
          backgroundColor={"green.500"}
          color="white"
        >
          New Chat
        </DrawerHeader>

        <DrawerBody backgroundColor={"white"}>
          <Stack spacing="24px">
            <Box>
              <FormLabel htmlFor="to" color="gray">
                To
              </FormLabel>
              <CheckboxGroup colorScheme="green">
                <Stack
                  spacing={[1]}
                  direction={["column"]}
                  maxHeight={"50vh"}
                  overflow={"auto"}
                  px={1}
                >
                  {contacts.map((contact) => (
                    <Checkbox
                      isChecked={checkedContactEmails.includes(contact.email)}
                      key={contact.id}
                      value={contact.id}
                      onChange={(e) => onCheckboxChange(e, contact.email)}
                    >
                      {contact.nickname}
                    </Checkbox>
                  ))}
                </Stack>
              </CheckboxGroup>
            </Box>

            <Box>
              <FormLabel htmlFor="message" color="gray">
                Message
              </FormLabel>
              <Textarea
                id="message"
                value={message}
                onChange={(e) => onMessageChange(e)}
                placeholder="Hey, what's up!"
                backgroundColor={"whitesmoke"}
              />
            </Box>
          </Stack>
        </DrawerBody>

        <DrawerFooter>
          <Button variant="outline" mr={3} onClick={() => onCancel()}>
            Cancel
          </Button>
          <Button
            colorScheme="green"
            disabled={!isValidMessage}
            onClick={() => onSend()}
          >
            Send
          </Button>
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
}
