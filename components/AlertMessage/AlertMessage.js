import { Alert, AlertIcon } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import styles from "./AlertMessage.module.css";

export default function AlertMessage({
  variant,
  message = "",
  delayDuration = 1000,
  alertDuration = 3500,
  onHide,
}) {
  const [show, setShow] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setShow(true);
      setTimeout(() => {
        onHide();
      }, alertDuration);
    }, delayDuration);
  }, [onHide, alertDuration, delayDuration]);

  if (!show) return null;

  return (
    <Alert status={variant} className={`AlertMessage ${styles.alertMessage}`}>
      <AlertIcon />
      {message}
    </Alert>
  );
}
