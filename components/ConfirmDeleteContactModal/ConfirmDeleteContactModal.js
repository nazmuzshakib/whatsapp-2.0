import React from "react";
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogCloseButton,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  Text,
} from "@chakra-ui/react";
import useContact from "../../hooks/useContact";

const ConfirmDeleteContactModal = ({ isOpen, onClose, contactToDelete }) => {
  const cancelRef = React.useRef();

  const { onDeleteContact } = useContact();

  console.log(contactToDelete);

  const onConfirmDeleteContact = () => {
    onDeleteContact(contactToDelete.id);
    onClose();
  };

  return (
    <>
      <AlertDialog
        motionPreset="slideInBottom"
        leastDestructiveRef={cancelRef}
        onClose={onClose}
        isOpen={isOpen}
        isCentered
      >
        <AlertDialogOverlay />
        <AlertDialogContent mx={3}>
          <AlertDialogHeader>Delete Contact?</AlertDialogHeader>
          <AlertDialogCloseButton />
          <AlertDialogBody>
            <Box
              fontSize={"1rem"}
              flexGrow={1}
              padding={2}
              backgroundColor="gray.100"
              borderRadius={"sm"}
            >
              <Text color={"red.400"}>
                <strong>{contactToDelete.nickname}</strong>
              </Text>
              <Text color="gray.500">
                <sup>{contactToDelete.email}</sup>
              </Text>
            </Box>
          </AlertDialogBody>
          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              No
            </Button>
            <Button
              colorScheme="red"
              ml={3}
              onClick={() => onConfirmDeleteContact()}
            >
              Yes
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    </>
  );
};

export default ConfirmDeleteContactModal;
