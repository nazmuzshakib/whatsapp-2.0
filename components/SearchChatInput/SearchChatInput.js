import { Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import React from "react";
import { AiOutlineSearch } from "react-icons/ai";

export default function SearchChatInput() {
  return (
    <form className={`SearchChatInput`}>
      <InputGroup>
        <InputLeftElement
          pointerEvents="none"
          // eslint-disable-next-line react/no-children-prop
          children={<AiOutlineSearch color="gray" />}
        />
        <Input
          type="text"
          backgroundColor={"white"}
          shadow={"sm"}
          placeholder="Search in chat..."
        />
      </InputGroup>
    </form>
  );
}
