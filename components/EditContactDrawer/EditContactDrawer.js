import {
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  FormLabel,
  Input,
  InputGroup,
  InputLeftAddon,
  InputRightAddon,
  Select,
  Stack,
  Textarea,
} from "@chakra-ui/react";
import React, { useState } from "react";
import useAuth from "../../hooks/useAuth";
import useContact from "../../hooks/useContact";

export default function EditContactDrawer({ isOpen, onClose, contactToEdit }) {
  const [nicknameInput, setNicknameInput] = useState(
    () => contactToEdit.nickname
  );
  const [emailInput, setEmailInput] = useState(() => contactToEdit.email);
  const { user, setUser } = useAuth();
  const { onUpdateContact } = useContact({
    user,
    setUser,
  });

  const isNicknameValid =
    nicknameInput &&
    typeof nicknameInput === "string" &&
    nicknameInput.length > 0;
  const isEmailValid =
    emailInput &&
    typeof emailInput === "string" &&
    /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(emailInput);
  const areFieldsValid = !!isNicknameValid && !!isEmailValid;
  const fieldsChanged =
    nicknameInput !== contactToEdit.nickname ||
    emailInput !== contactToEdit.email;

  const updateContact = () => {
    const updatedContact = {
      nickname: nicknameInput,
      email: emailInput,
    };
    onUpdateContact(contactToEdit.id, updatedContact);
    onClose();
  };

  return (
    <Drawer isOpen={isOpen} placement="bottom" onClose={onClose}>
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton color={"white"} />
        <DrawerHeader
          borderBottomWidth="1px"
          backgroundColor={"green.500"}
          color="white"
        >
          New Contact
        </DrawerHeader>

        <DrawerBody>
          <Stack spacing="24px">
            <Box>
              <FormLabel htmlFor="username" color={"gray"}>
                Nickname
              </FormLabel>
              <Input
                id="name"
                placeholder="Bobbie"
                value={nicknameInput}
                onChange={(e) => setNicknameInput(e.target.value)}
                required
              />
            </Box>

            <Box>
              <FormLabel htmlFor="email" color={"gray"}>
                Email
              </FormLabel>
              <InputGroup>
                <InputLeftAddon>@</InputLeftAddon>
                <Input
                  type="email"
                  id="email"
                  placeholder="bobbie@lol.com"
                  value={emailInput}
                  onChange={(e) =>
                    setEmailInput(e.target.value.trim().toLowerCase())
                  }
                  required
                />
              </InputGroup>
            </Box>
          </Stack>
        </DrawerBody>

        <DrawerFooter>
          <Button variant="outline" mr={3} onClick={onClose}>
            Cancel
          </Button>
          <Button
            colorScheme="green"
            disabled={!areFieldsValid || !fieldsChanged}
            onClick={() => updateContact()}
          >
            Save
          </Button>
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  );
}
