import {
  Box,
  Button,
  Flex,
  Heading,
  StackDivider,
  Text,
  useDisclosure,
  VStack,
} from "@chakra-ui/react";
import React, { useState } from "react";
import {
  AiOutlineDelete,
  AiOutlineEdit,
  AiOutlineMessage,
} from "react-icons/ai";
import useAuth from "../../hooks/useAuth";
import ConfirmDeleteContactModal from "../ConfirmDeleteContactModal/ConfirmDeleteContactModal";
import EditContactDrawer from "../EditContactDrawer/EditContactDrawer";

export default function Contacts() {
  const {
    isOpen: isEditContactDrawerOpen,
    onOpen: onEditContactDrawerOpen,
    onClose: onEditContactDrawerClose,
  } = useDisclosure();
  const {
    isOpen: isDeleteContactModalOpen,
    onOpen: onDeleteContactModalOpen,
    onClose: onDeleteContactModalClose,
  } = useDisclosure();

  const [contactToEdit, setContactToEdit] = useState(null);
  const [contactToDelete, setContactToDelete] = useState(null);

  const { user = {} } = useAuth();
  const { contacts = [] } = user;

  return (
    <>
      <Flex className="Contacts" flexDirection={"column"}>
        <Heading as="h1" size="xl">
          Contacts
        </Heading>

        <VStack
          my={4}
          divider={<StackDivider borderColor="white.100" />}
          spacing={3}
          align="stretch"
        >
          {contacts.map((contact) => (
            <Box key={`${contact.id}`}>
              <Flex alignItems={"flex"}>
                <Box fontSize={"1rem"} flexGrow={1}>
                  <Text color={"red.400"}>
                    <strong>{contact.nickname}</strong>
                  </Text>
                  <Text color="gray.500">
                    <sup>{contact.email}</sup>
                  </Text>
                </Box>
                <Button size={"sm"} variant="ghost">
                  <AiOutlineMessage size={"1.25rem"} color="green" />
                </Button>
                <Button
                  size={"sm"}
                  variant="ghost"
                  onClick={() => {
                    setContactToEdit(contact);
                    onEditContactDrawerOpen();
                  }}
                >
                  <AiOutlineEdit size={"1.25rem"} color="orange" />
                </Button>
                <Button
                  size={"sm"}
                  variant="ghost"
                  onClick={() => {
                    setContactToDelete(contact);
                    onDeleteContactModalOpen();
                  }}
                >
                  <AiOutlineDelete size={"1.25rem"} color="red" />
                </Button>
              </Flex>
            </Box>
          ))}
        </VStack>
      </Flex>

      {contactToEdit && (
        <EditContactDrawer
          isOpen={isEditContactDrawerOpen}
          onOpen={onEditContactDrawerOpen}
          onClose={onEditContactDrawerClose}
          contactToEdit={contactToEdit}
        />
      )}

      {contactToDelete && (
        <ConfirmDeleteContactModal
          isOpen={isDeleteContactModalOpen}
          onOpen={onDeleteContactModalOpen}
          onClose={onDeleteContactModalClose}
          contactToDelete={contactToDelete}
        />
      )}
    </>
  );
}
