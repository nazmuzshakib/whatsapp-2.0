import { useState, useEffect } from "react";
import Head from "next/head";
import Login from "../components/Login/Login";
import useAuth from "../hooks/useAuth";
import useView, {
  CHATS_VIEW,
  CONTACTS_VIEW,
  LOADING_VIEW,
  LOGIN_VIEW,
} from "../hooks/useView";
import Dashboard from "../components/Dashboard/Dashboard";
import AlertMessage from "../components/AlertMessage/AlertMessage";
import Contacts from "../components/Contacts/Contacts";
import Chats from "../components/Chats/Chats";
import { CircularProgress } from "@chakra-ui/react";

export default function Home() {
  const {
    user,
    isAuthenticated,
    isLoading,
    error,
    onLoginWithGoogle,
    onLogout,
  } = useAuth();
  const [errorAlert, setErrorAlert] = useState(null);
  // const [successAlert, setSuccessAlert] = useState(null);

  const { activeView, goToChatDashboardView, goToContactDashboardView } =
    useView({
      isAuthenticated,
    });

  const renderView = () => {
    if (activeView === LOADING_VIEW) {
      return (
        <CircularProgress
          isIndeterminate
          color="green.300"
          size={"40vw"}
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          }}
        />
      );
    }
    if (activeView === LOGIN_VIEW) {
      return (
        <Login
          onLoginWithGoogle={onLoginWithGoogle}
          isLoadingAuth={isLoading}
        />
      );
    }
    return (
      <Dashboard
        onLogout={onLogout}
        activeView={activeView}
        goToChatDashboardView={goToChatDashboardView}
        goToContactDashboardView={goToContactDashboardView}
      >
        {activeView === CHATS_VIEW ? <Chats /> : null}
        {activeView === CONTACTS_VIEW ? <Contacts /> : null}
      </Dashboard>
    );
  };

  useEffect(() => {
    if (error !== null) setErrorAlert(error);
  }, [error]);

  // useEffect(() => {
  //   if (user?.displayName || user?.email) {
  //     setSuccessAlert(`Hey, ${user?.displayName || user?.email}!`);
  //   }
  // }, [user?.displayName, user?.email]);

  return (
    <>
      <Head>
        <title>WhatsApp 2.0</title>
        <meta name="description" content="A free chat app like WhatsApp" />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="apple-touch-icon"
          sizes="57x57"
          href="/apple-icon-57x57.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="60x60"
          href="/apple-icon-60x60.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="/apple-icon-72x72.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="76x76"
          href="/apple-icon-76x76.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="114x114"
          href="/apple-icon-114x114.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="120x120"
          href="/apple-icon-120x120.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href="/apple-icon-144x144.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="152x152"
          href="/apple-icon-152x152.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-icon-180x180.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href="/android-icon-192x192.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="96x96"
          href="/favicon-96x96.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/manifest.json" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
        <meta name="theme-color" content="#ffffff"></meta>
      </Head>
      <main>
        {errorAlert ? (
          <AlertMessage
            variant="error"
            message={errorAlert}
            onHide={() => setErrorAlert(null)}
          />
        ) : null}

        {/* {successAlert ? (
          <AlertMessage
            variant="success"
            message={successAlert}
            onHide={() => setSuccessAlert(null)}
          />
        ) : null} */}

        {renderView()}
      </main>
    </>
  );
}
