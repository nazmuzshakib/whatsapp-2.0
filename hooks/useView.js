import { useEffect, useState } from "react";

export const LOGIN_VIEW = "LOGIN_VIEW";
export const CHATS_VIEW = "CHATS_VIEW";
export const CONTACTS_VIEW = "CONTACTS_VIEW";
export const LOADING_VIEW = "LOADING_VIEW";

export default function useView({ isAuthenticated }) {
  const [activeView, setActiveView] = useState(LOADING_VIEW);

  console.log(activeView);

  useEffect(() => {
    setActiveView(isAuthenticated ? CHATS_VIEW : LOGIN_VIEW);
  }, [isAuthenticated]);

  const goToLoginView = () => setActiveView(LOGIN_VIEW);
  const goToChatDashboardView = () => setActiveView(CHATS_VIEW);
  const goToContactDashboardView = () => setActiveView(CONTACTS_VIEW);

  return {
    activeView,
    goToLoginView,
    goToChatDashboardView,
    goToContactDashboardView,
  };
}
