import * as DB from "../services/firebase/firestore";
import useAuth from "./useAuth";

const useChat = () => {
  const { user, setUser } = useAuth();

  const { email: activeUserEmail } = user || {};

  const onCreateNewChat = async (newChat) => {
    const { error: createNewChatError } = await DB.createNewChat({
      ...newChat,
    });

    if (createNewChatError) {
      return { error: "Unable to create a new chat at the moment" };
    }

    const contactEmailInformers = (newChat?.receiverEmails || []).map(
      async (contactEmail) => {
        return new Promise(async (resolve) => {
          const { user: fetchedUser, error: getUserError } =
            await DB.getUserByEmail(contactEmail);

          if (getUserError)
            return resolve({ invalidContactEmail: true, contactEmail });

          const { error: updateUserChatIdError } = await DB.updateUser(
            contactEmail,
            {
              ...fetchedUser,
              chatIds: [...(fetchedUser?.chatIds || []), newChat.id],
            }
          );

          if (updateUserChatIdError) {
            return resolve({ chatDeliveryFailed: true, contactEmail });
          }

          return resolve({ chatDeliverySuccess: true, contactEmail });
        });
      }
    );

    const receiverConfirmations = await Promise.all(contactEmailInformers);
    console.log({ receiverConfirmations });

    const { user: updatedUser, error: updatedUserError } =
      await DB.getUserByEmail(activeUserEmail);
    if (updatedUserError)
      console.log("Issue refetching user after creating new chat");

    setUser(updatedUser);
  };

  // const onUpdateContact = async (contactId, updatedContact) => {
  //   const { error } = await DB.updateUser(activeUserEmail, {
  //     ...user,
  //     contacts: contacts.map((contact) => {
  //       if (contact.id === contactId) {
  //         return {
  //           ...updatedContact,
  //         };
  //       }
  //       return contact;
  //     }),
  //   });

  //   if (error) console.log("Unable to update contact at the moment.");

  //   const { user: updatedUser, error: updatedUserError } =
  //     await DB.getUserByEmail(activeUserEmail);
  //   if (updatedUserError)
  //     console.log("Issue refetching user after updating contact");

  //   setUser(updatedUser);
  // };

  // const onDeleteContact = async (contactId) => {
  //   const { error } = await DB.updateUser(activeUserEmail, {
  //     ...user,
  //     contacts: contacts.filter((contact) => contact.id !== contactId),
  //   });

  //   if (error) console.log("Unable to update contact at the moment.");

  //   const { user: updatedUser, error: updatedUserError } =
  //     await DB.getUserByEmail(activeUserEmail);
  //   if (updatedUserError)
  //     console.log("Issue refetching user after updating contact");

  //   setUser(updatedUser);
  // };

  const getChatById = async (chatId) => await DB.getChatById(chatId);

  const getAllChats = () => {};

  return {
    // onCreateNewContact,
    getAllChats,
    onCreateNewChat,
    getChatById,
    // onUpdateContact,
    // onDeleteContact,
  };
};

export default useChat;
