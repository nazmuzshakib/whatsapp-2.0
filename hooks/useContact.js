import * as DB from "../services/firebase/firestore";
import useAuth from "./useAuth";

const useContact = () => {
  const { user, setUser } = useAuth();

  const { contacts = [], email: activeUserEmail } = user || {};

  const onCreateNewContact = async (newContact) => {
    const { error } = await DB.updateUser(activeUserEmail, {
      ...user,
      contacts: [...contacts, { ...newContact }],
    });
    if (error) console.log("Unable to save a new contact at the moment.");

    const { user: updatedUser, error: updatedUserError } =
      await DB.getUserByEmail(activeUserEmail);
    if (updatedUserError)
      console.log("Issue refetching user after creating new contact");

    setUser(updatedUser);
  };

  const onUpdateContact = async (contactId, updatedContact) => {
    const { error } = await DB.updateUser(activeUserEmail, {
      ...user,
      contacts: contacts.map((contact) => {
        if (contact.id === contactId) {
          return {
            ...updatedContact,
          };
        }
        return contact;
      }),
    });

    if (error) console.log("Unable to update contact at the moment.");

    const { user: updatedUser, error: updatedUserError } =
      await DB.getUserByEmail(activeUserEmail);
    if (updatedUserError)
      console.log("Issue refetching user after updating contact");

    setUser(updatedUser);
  };

  const onDeleteContact = async (contactId) => {
    const { error } = await DB.updateUser(activeUserEmail, {
      ...user,
      contacts: contacts.filter((contact) => contact.id !== contactId),
    });

    if (error) console.log("Unable to update contact at the moment.");

    const { user: updatedUser, error: updatedUserError } =
      await DB.getUserByEmail(activeUserEmail);
    if (updatedUserError)
      console.log("Issue refetching user after updating contact");

    setUser(updatedUser);
  };

  const getAllContacts = () => contacts;

  return {
    onCreateNewContact,
    getAllContacts,
    onUpdateContact,
    onDeleteContact,
  };
};

export default useContact;
